const path = require('path')
const Koa = require('koa')
const views = require('koa-views')
const koaStatic = require('koa-static')
const bodyParser = require('koa-bodyparser')
const routers = require('./routers/index')

// 创建一个Koa对象表示web app本身:
const app = new Koa();

app.use(bodyParser())

app.use(koaStatic(
  path.join(__dirname , './../static')
))

app.use(views(path.join(__dirname, './views'), {
  extension: 'ejs'
}))
// 对于任何请求，app将调用该异步函数处理请求：
app.use(routers.routes()).use(routers.allowedMethods())

// 在端口3000监听:
app.listen(3000);
console.log('app started at port 3000...');
